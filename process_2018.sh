#!/bin/sh

# download source JP2 imagery (4GB ZIP, 4GB unzipped)
echo "download source jp2"
wget --quiet https://s3-ap-southeast-2.amazonaws.com/opendata-aerial-imagery/mga55_gda94_10cm_2018_CoM_true_ortho.zip
echo "unzip"
unzip mga55_gda94_10cm_2018_CoM_true_ortho.zip
rm mga55_gda94_10cm_2018_CoM_true_ortho.zip

# convert from JP2 to uncompressed GeoTIFF 21GB (you can use DEFLATE if you're constrained by disk space)
# Takes 2 mins on 24CPUs using about 20GB RAM
# This is run with https://hub.docker.com/r/klokantech/gdal/ which has an more JP2 drivers compared to standard debian
# since this file will fail with JP2OpenJPEG which is the only option on Debian GDAL
# the source alpha band is not very useful as it doesn't distinguish the background, so drop it
docker run --rm -v $(pwd):/data klokantech/gdal gdal_translate \
  -of GTiff \
  -co COMPRESS=NONE \
  -co TILED=YES \
  -co SPARSE_OK=TRUE \
  -co BIGTIFF=IF_SAFER \
  -co COPY_SRC_OVERVIEWS=NO \
  --config GDAL_NUM_THREADS ALL_CPUS \
  --config GDAL_CACHEMAX 80% \
  -b 1 -b 2 -b 3 \
  mga55_gda94_10cm_2018_CoM_true_ortho/mga55_gda94_10cm_2018_CoM_true_ortho.jp2 \
  mga55_gda94_10cm_2018_CoM_true_ortho.tiff

rm -rf mga55_gda94_10cm_2018_CoM_true_ortho

# use nearblack to create a mask to clip out the background
# takes about 6 mins with the output 12GB
echo "create mask with nearblack"
nearblack \
  -of GTiff \
  -co COMPRESS=NONE \
  -co TILED=YES \
  -co SPARSE_OK=TRUE \
  -co BIGTIFF=IF_SAFER \
  --config GDAL_NUM_THREADS ALL_CPUS \
  --config GDAL_CACHEMAX 50% \
  -color 127,129,253 \
  -color 0,0,0 \
  -setmask \
  -near 30 \
  -alg twopasses \
  -o mga55_gda94_10cm_2018_CoM_true_ortho.mask.tiff \
  mga55_gda94_10cm_2018_CoM_true_ortho.tiff

rm -f mga55_gda94_10cm_2018_CoM_true_ortho

# create a Cloud Optimized GeoTIFF (878MB)
# convert to COG which also reprojects to EPSG:3857
# more CPUs will make this go faster
# takes about 22 mins on 24CPUs
# based on https://github.com/linz/basemaps/blob/master/docs/operator-guide/cog-quality.md
gdal_translate \
  -of COG \
  -co COMPRESS=webp \
  -co NUM_THREADS=ALL_CPUS \
  -co BIGTIFF=YES \
  -co TILING_SCHEME=GoogleMapsCompatible \
  --config BIGTIFF_OVERVIEW YES \
  -co ALIGNED_LEVELS=1 \
  -co SPARSE_OK=TRUE \
  -co ADD_ALPHA=YES \
  -co BLOCKSIZE=256 \
  -co WARP_RESAMPLING=bilinear \
  -co OVERVIEW_RESAMPLING=lanczos \
  -co QUALITY=70 \
  -co ZOOM_LEVEL_STRATEGY=UPPER \
  --config GDAL_NUM_THREADS ALL_CPUS \
  --config GDAL_CACHEMAX 50% \
   mga55_gda94_10cm_2018_CoM_true_ortho.mask.tiff \
  CoM_May2018_10cm.cog.tiff

rm -f mga55_gda94_10cm_2018_CoM_true_ortho.mask.tiff

echo "upload result to b2"
b2 upload_file $B2_BUCKET CoM_May2018_10cm.cog.tiff CoM_May2018_10cm.cog.tiff
