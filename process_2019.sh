#!/bin/sh

# download source ECW imagery
echo "download source ecw"
wget --quiet https://opendatasoft-s3.s3.ap-southeast-2.amazonaws.com/attachments/2019_Aerial_Imagery/AerialImagery_190203.ecw

# convert from ECW to uncompressed GeoTIFF 56GB (you can use DEFLATE if you're constrained by disk space)
# Takes 2 mins on 24CPUs using about 50GB RAM
# This is run with https://hub.docker.com/r/klokantech/gdal/ which an ECW driver
docker run --rm -v $(pwd):/data klokantech/gdal gdal_translate \
  -of GTiff \
  -co COMPRESS=NONE \
  -co TILED=YES \
  -co SPARSE_OK=TRUE \
  -co BIGTIFF=IF_SAFER \
  -co NUM_THREADS=ALL_CPUS \
  -co COPY_SRC_OVERVIEWS=NO \
  --config GDAL_NUM_THREADS ALL_CPUS \
  --config GDAL_CACHEMAX 80% \
  -b 1 -b 2 -b 3 \
  AerialImagery_190203.ecw \
  AerialImagery_190203.tiff

rm -f AerialImagery_190203.ecw

# use nearblack to create a mask to clip out the background
# output is 43GB
echo "create mask with nearblack"
nearblack \
  -of GTiff \
  -co COMPRESS=NONE \
  -co TILED=YES \
  -co SPARSE_OK=TRUE \
  -co BIGTIFF=IF_SAFER \
  --config GDAL_NUM_THREADS ALL_CPUS \
  --config GDAL_CACHEMAX 50% \
  -color 255,255,255 \
  -color 0,0,0 \
  -setmask \
  -near 30 \
  -alg twopasses \
  -o AerialImagery_190203.mask.tiff \
  AerialImagery_190203.tiff

rm -f AerialImagery_190203.tiff

# create a Cloud Optimized GeoTIFF (755MB)
# convert to COG which also reprojects to EPSG:3857
# more CPUs will make this go faster
# takes about 2.5hrs on 1CPU
# based on https://github.com/linz/basemaps/blob/master/docs/operator-guide/cog-quality.md
gdal_translate \
  -of COG \
  -co COMPRESS=webp \
  -co NUM_THREADS=ALL_CPUS \
  -co BIGTIFF=YES \
  -co TILING_SCHEME=GoogleMapsCompatible \
  --config BIGTIFF_OVERVIEW YES \
  -co ALIGNED_LEVELS=1 \
  -co SPARSE_OK=TRUE \
  -co ADD_ALPHA=YES \
  -co BLOCKSIZE=256 \
  -co WARP_RESAMPLING=bilinear \
  -co OVERVIEW_RESAMPLING=lanczos \
  -co QUALITY=70 \
  -co ZOOM_LEVEL_STRATEGY=UPPER \
  --config GDAL_NUM_THREADS ALL_CPUS \
  --config GDAL_CACHEMAX 50% \
  AerialImagery_190203.mask.tiff \
  CoM_03Feb2019.cog.tiff

rm -f AerialImagery_190203.mask.tiff

echo "upload result to b2"
b2 upload_file $B2_BUCKET CoM_03Feb2019.cog.tiff CoM_03Feb2019.cog.tiff
