FROM debian:trixie-slim
RUN apt-get -y update && apt-get -y install wget pipx gdal-bin docker.io && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN pipx ensurepath && pipx install --include-deps rio-cogeo && pipx install b2 && rm -rf /root/.cache/pip*
ENV PATH="$PATH:/root/.local/bin"
