# city-of-melbourne-cogeo

The City of Melbourne publish Aerial Imagery on their [open data portal](https://data.melbourne.vic.gov.au/) for

- [2018](https://data.melbourne.vic.gov.au/explore/dataset/2018-aerial-imagery-true-ortho/information/)
- [2019](https://data.melbourne.vic.gov.au/explore/dataset/2019-aerial-imagery/information/)
- [2020](https://data.melbourne.vic.gov.au/explore/dataset/2020-aerial-imagery-true-ortho/information/)

This repository processes the published imagery for use by OpenStreetMap editors.
