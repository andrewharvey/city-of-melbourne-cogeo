#!/bin/sh

# download source ECW imagery 46GB
echo "download source ecw"
wget --quiet https://opendata-aerial-imagery.s3.ap-southeast-2.amazonaws.com/Taylors_May2020_2cm_GDA2020z55.jp2

# convert from JP2 to GeoTIFF 234GB
# This is run with https://hub.docker.com/r/klokantech/gdal/ which has an more JP2 drivers compared to standard debian
# since this file will fail with JP2OpenJPEG which is the only option on Debian GDAL
# This should consume all available CPUs and took < 1hr with 24CPUs
docker run --rm -v $(pwd):/data klokantech/gdal gdal_translate \
  -of GTiff \
  -co COMPRESS=DEFLATE \
  -co TILED=YES \
  -co BIGTIFF=IF_SAFER \
  -co COPY_SRC_OVERVIEWS=NO \
  -co NUM_THREADS=ALL_CPUS \
  --config GDAL_NUM_THREADS ALL_CPUS \
  --config GDAL_CACHEMAX 80% \
  Taylors_May2020_2cm_GDA2020z55.jp2 \
  Taylors_May2020_2cm_GDA2020z55.tiff

# cleanup no longer needed
rm -f Taylors_May2020_2cm_GDA2020z55.jp2

# nearblack to create a clean mask
# floodfill is slightly better around Webb Dock, but also slightly slower
# twopasses is good enough and slightly faster
# seems to be bound by a single thread, took 10h on a single thread 1GB ram
nearblack \
  -of GTiff \
  -co COMPRESS=DEFLATE \
  -co TILED=YES \
  -co SPARSE_OK=TRUE \
  -co BIGTIFF=IF_SAFER \
  --config GDAL_NUM_THREADS ALL_CPUS \
  --config GDAL_CACHEMAX 80% \
  -color 255,255,255 \
  -color 0,0,0 \
  -setmask \
  -near 15 \
  -alg twopasses \
  -o Taylors_May2020_2cm_GDA2020z55.mask.tiff \
  Taylors_May2020_2cm_GDA2020z55.tiff

# cleanup no longer needed
rm -f Taylors_May2020_2cm_GDA2020z55.tiff

# convert to COG which also reprojects to EPSG:3857
# more CPUs will make this go faster
# based on https://github.com/linz/basemaps/blob/master/docs/operator-guide/cog-quality.md
gdal_translate \
  -of COG \
  -co COMPRESS=webp \
  -co NUM_THREADS=ALL_CPUS \
  -co BIGTIFF=YES \
  -co TILING_SCHEME=GoogleMapsCompatible \
  --config BIGTIFF_OVERVIEW YES \
  -co ALIGNED_LEVELS=1 \
  -co SPARSE_OK=TRUE \
  -co ADD_ALPHA=YES \
  -co BLOCKSIZE=256 \
  -co WARP_RESAMPLING=bilinear \
  -co OVERVIEW_RESAMPLING=lanczos \
  -co QUALITY=50 \
  -co ZOOM_LEVEL_STRATEGY=UPPER \
  --config GDAL_NUM_THREADS ALL_CPUS \
  --config GDAL_CACHEMAX 20% \
  Taylors_May2020_2cm_GDA2020z55.mask.tiff \
  CoM_May2020_2cm.cog.tiff

echo "upload result to b2"
b2 upload_file $B2_BUCKET CoM_May2020_2cm.cog.tiff CoM_May2020_2cm.cog.tiff
